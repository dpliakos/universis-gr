import { Link } from "gatsby";
import PropTypes from "prop-types";
import React, {useState} from "react";
import { HEADER_TEXTS } from "../assets/data/languages";

const Header = props => {
  const [isOpen, setIsOpen] = useState(false);
  return (<div className="nav-container" id="start">
    <div className="bar bar--sm visible-xs">
      <div className="container">
        <div className="row">
          <div className="col-12 col-md-12 text-right">
            <a
                href="#"
                className="hamburger-toggle"
                onClick={event => {
                  event.preventDefault();
                  setIsOpen(!isOpen);
                }}
            >
              <i className="icon icon--sm stack-interface stack-menu"/>
            </a>
          </div>
        </div>
      </div>
    </div>

    <nav id="menu1" className={isOpen ? "bar bar--sm bar-1" : "bar bar--sm bar-1 hidden-xs"}>
      <div className="container">
        <div className="row">
          <div className="col-lg-11 col-md-12 text-right text-left-xs text-left-sm">
            <div className="bar__module">
              <ul className="menu-horizontal text-left">
                <li className="dropdown">
                  <a href="#action" className="scroll-link">
                    <span className="nav-item-custom">
                      {HEADER_TEXTS.action[props.lang]}
                    </span>
                  </a>
                </li>
                <li className="dropdown">
                  <a href="#chars" className="scroll-link">
                    <span className="nav-item-custom">
                      {HEADER_TEXTS.chars[props.lang]}
                    </span>
                  </a>
                </li>
                <li className="dropdown">
                  <a href="#trial" className="scroll-link">
                    <span className="nav-item-custom">
                      {HEADER_TEXTS.trial[props.lang]}
                    </span>
                  </a>
                </li>
                <li className="dropdown">
                  <a href="#participate" className="scroll-link">
                    <span className="nav-item-custom">
                      {HEADER_TEXTS.contibute[props.lang]}
                    </span>
                  </a>
                </li>
                <li className="dropdown">
                  <a className="scroll-link" onClick={props.changeLang}>
                    {" "}
                    <span className="nav-item-custom">
                      {" "}
                      {HEADER_TEXTS.language[props.lang]}
                    </span>
                  </a>
                </li>
              </ul>
            </div>

            <div className="bar__module">
              <a
                  className="btn btn--primary type--uppercase"
                  href="mailto:info@universis.gr?Subject=Πληροφορίες για UniverSIS"
              >
                <span className="btn__text">
                  {HEADER_TEXTS.contact[props.lang]}
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>)
};

Header.propTypes = {
  siteTitle: PropTypes.string
};

Header.defaultProps = {
  siteTitle: ``
};

export default Header;
