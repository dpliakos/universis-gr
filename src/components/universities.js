import { Link } from "gatsby";
import PropTypes from "prop-types";
import React, { Fragment } from "react";
import Auth from "../assets/images/auth.png";
import Duth from "../assets/images/duth.png";
import { UNIVERSITIES_TEXTS } from "../assets/data/languages";

const Universities = props => (
  <Fragment>
    <section className="text-center ">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-10 col-lg-8">
            <h2>{UNIVERSITIES_TEXTS.title[props.lang]}</h2>
            <p className="lead">{UNIVERSITIES_TEXTS.subtitle[props.lang]}</p>
          </div>
        </div>
      </div>
    </section>
    <section className="text-center">
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <div className="feature feature-8">
              <img alt="Image" src={Auth} />
              <h5>{UNIVERSITIES_TEXTS.auth[props.lang]}</h5>
            </div>
          </div>
          <div className="col-md-4">
            <div className="feature feature-8">
              <img alt="Image" src={Duth} />
              <h5> {UNIVERSITIES_TEXTS.duth[props.lang]}</h5>
            </div>
          </div>
          <div className="col-md-4">
            <div className="feature feature-8 member-action">
              <a className="btn btn--primary type--uppercase " href="#contact">
                <span className="btn__text">
                  {" "}
                  {UNIVERSITIES_TEXTS.cta[props.lang]}
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </Fragment>
);

export default Universities;
