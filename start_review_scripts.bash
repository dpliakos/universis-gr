#!/bin/bash -ex

curl -X POST https://api.heroku.com/teams/apps -H "Accept: application/vnd.heroku+json; version=3" -H "Authorization: Bearer $HEROKU_API_KEY" -H "Content-Type: application/json" -d "{\"name\":\"review-$CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG\",\"region\":\"eu\"}"

git checkout -B $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG

git config user.email 'stellarouzi@it.auth.gr'
git config user.name 'Stella Rouzi'

git add .
git commit -m "Ready to push latest changes to new heroku review app"
git push -f https://heroku:$HEROKU_API_KEY@git.heroku.com/review-$CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG.git $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG:master
